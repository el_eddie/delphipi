DelphiPI
========

Delphi Package Installer


Delphi Package Installer (DelphiPI) is a tool which aids you installing components to your Delphi IDE. DelphiPI automatically resolves dependencies between packages, compiles, installs and adds source paths to your IDE. 

### How to compile

You need to download and install two packages to be able to compile DelphiPI.

  * Download and install [JCL 2.5](https://github.com/project-jedi/jcl). Version used is JCL 2.5
  * Download and install [Softgem's Virtual TreeView](www.soft-gems.net). Instructions for installing are in the INSTALL.txt file. Version used is 5.1.3
  * Open DelphiPI Package Group and build.
